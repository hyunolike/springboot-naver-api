# Spring Boot + Naver API
> Thymleaf + Naver API

## 목차
1. [기본 설정](#1-기본-설정)
2. [API 개발](#2-API-개발)
3. [프로젝트 구조](#3-프로젝트-구조)

## 1 기본 설정
1. `entity` `repository` 개발
2. `unit test`
```java
@SpringBootTest
class WishListRepositoryTest {

    @Autowired
    private WishListRepository wishListRepository;

    private WishListEntity create(){
        WishListEntity wishListEntity = WishListEntity.builder()
                .title("제목")
                .category("카테고리")
                .address("주소")
                .roadAddress("도로 주소")
                .homePageLink("")
                .imageLink("")
                .isVisit(false)
                .visitCount(10)
                .lastVisitDate(LocalDateTime.now())
                .build();
        return wishListEntity;
    }


    @Test
    @DisplayName("저장 테스트")
    public void saveTest(){
        var wishListEntity = create();
        var expected = wishListRepository.save(wishListEntity);

        Assertions.assertNotNull(expected);
        Assertions.assertEquals(1, expected.getIndex());

    }

    @Test
    @DisplayName("수정 테스트")
    public void updateTest(){
        var wishListEntity = create();
        var expected = wishListRepository.save(wishListEntity);

        expected.setTitle("update test");
        var saveEntity = wishListRepository.save(expected);

        Assertions.assertEquals("update test", saveEntity.getTitle());
        Assertions.assertEquals(1, wishListRepository.listAll().size());



    }

    @Test
    @DisplayName("조회 테스트")
    public void findByIdTest(){
        var wishListEntity = create();
        wishListRepository.save(wishListEntity);

        var expected = wishListRepository.findById(1);

        Assertions.assertEquals(true, expected.isPresent()); // 값이 존재하면
        Assertions.assertEquals(1, expected.get().getIndex());
    }

    @Test
    @DisplayName("삭제 테스트 ")
    public void deleteTest(){
        var wishListEntity = create();
        wishListRepository.save(wishListEntity);

        wishListRepository.deleteById(1);

        int count = wishListRepository.listAll().size();

        Assertions.assertEquals(0, count);
    }

    @Test
    @DisplayName("전체 조회 테스트")
    public void listAllTest(){
        var wishListEntity1 = create();
        wishListRepository.save(wishListEntity1);

        var wishListEntity2 = create();
        wishListRepository.save(wishListEntity2);


        int count = wishListRepository.listAll().size();
        Assertions.assertEquals(2, count);
    }

}
```

## 2 API 개발
### 1. `NAVER API` 연동
    1. `.yaml` 파일 클라이언트 키, 비밀 키
    2. `dto` & `naver client` 개발
    3. `unit test`

### ⭐ HTTP & REST
```java
public SearchLocalRes localSearch(SearchLocalReq searchLocalReq){
    var uri = UriComponentsBuilder.fromUriString(naverLocalSearchUrl)
            .queryParams(searchLocalReq.toMultiValueMap())
            .build()
            .encode()
            .toUri();

    // header setting
    var headers = new HttpHeaders();
    headers.set("X-Naver-Client-Id",naverClientId);
    headers.set("X-Naver-Client-Secret",naverSecret);
    headers.setContentType(MediaType.APPLICATION_JSON);


    // 요청 >> entity
    var httpEntity = new HttpEntity<>(headers);
    var responseType = new ParameterizedTypeReference<SearchLocalRes>(){};

    // rest templete
    var responseEntity = new RestTemplate().exchange(
            uri,
            HttpMethod.GET,
            httpEntity,
            responseType
    );

    return responseEntity.getBody();
}
```

### 2. `service` 개발
   1. service & dto
   2. unit test
   ```
   WishListDto(index=0, title=애플하우스, category=분식>떡볶이, address=서울특별시 서초구...   
   ```
![](img/main.jpg)

### 3. `controller` 개발
   1. controller (검색, 위시 리스트 추가, 전체 조회, 방문 체크, 삭제)
   2. service 메서드 추가
   3. `swaagger` 를 통한 api 테스트

<details>
<summary>검색 api 결과</summary>
<div markdown="1">

![](img/search.jpg)
</div>
</details>
<details>
<summary>위시리스트 추가 api 결과</summary>
<div markdown="1">

![](img/wishlist.jpg)
</div>
</details>
<details>
<summary>전체 조회 api 결과</summary>
<div markdown="1">

![](img/all.jpg)
</div>
</details>
<details>
<summary>방문 체크 api 결과</summary>
<div markdown="1">

![](img/visit.jpg)
</div>
</details>

## 3 프로젝트 구조

```
📦 프로젝트
├─ 메인
│  ├─ controller 
│  │  ├─ 서버 api
│  │  └─ 뷰단 api
│  ├─ db
│  │  ├─ 메모리 엔티티
│  │  ├─ 메모리 레포지토리
│  │  └─ 메모리 레포지토리 추상화
│  ├─ naver
│  │  ├─ dto
│  │  │  ├─ 검색 이미지 요청
│  │  │  ├─ 검색 이미지 응답
│  │  │  ├─ 검색 지역 요청
│  │  │  └─ 검색 지역 응답
│  │  └─ naver client 😊 네이버 API 연결 
│  ├─ restaurant
│  │  ├─ dto
│  │  ├─ entity
│  │  ├─ repository
│  │  └─ service
│  └─ 리소스
│     ├─ static
│     │  └─ main.js
│     ├─ templete
│     │  └─ main.html
│     └─ application.yaml
└─ 테스트
```
