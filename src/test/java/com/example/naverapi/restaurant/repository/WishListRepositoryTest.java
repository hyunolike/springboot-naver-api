package com.example.naverapi.restaurant.repository;

import com.example.naverapi.restaurant.entity.WishListEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class WishListRepositoryTest {

    @Autowired
    private WishListRepository wishListRepository;

    private WishListEntity create(){
        WishListEntity wishListEntity = WishListEntity.builder()
                .title("제목")
                .category("카테고리")
                .address("주소")
                .roadAddress("도로 주소")
                .homePageLink("")
                .imageLink("")
                .isVisit(false)
                .visitCount(10)
                .lastVisitDate(LocalDateTime.now())
                .build();
        return wishListEntity;
    }


    @Test
    @DisplayName("저장 테스트")
    public void saveTest(){
        var wishListEntity = create();
        var expected = wishListRepository.save(wishListEntity);

        Assertions.assertNotNull(expected);
        Assertions.assertEquals(1, expected.getIndex());

    }

    @Test
    @DisplayName("수정 테스트")
    public void updateTest(){
        var wishListEntity = create();
        var expected = wishListRepository.save(wishListEntity);

        expected.setTitle("update test");
        var saveEntity = wishListRepository.save(expected);

        Assertions.assertEquals("update test", saveEntity.getTitle());
        Assertions.assertEquals(1, wishListRepository.listAll().size());



    }

    @Test
    @DisplayName("조회 테스트")
    public void findByIdTest(){
        var wishListEntity = create();
        wishListRepository.save(wishListEntity);

        var expected = wishListRepository.findById(1);

        Assertions.assertEquals(true, expected.isPresent()); // 값이 존재하면
        Assertions.assertEquals(1, expected.get().getIndex());
    }

    @Test
    @DisplayName("삭제 테스트 ")
    public void deleteTest(){
        var wishListEntity = create();
        wishListRepository.save(wishListEntity);

        wishListRepository.deleteById(1);

        int count = wishListRepository.listAll().size();

        Assertions.assertEquals(0, count);
    }

    @Test
    @DisplayName("전체 조회 테스트")
    public void listAllTest(){
        var wishListEntity1 = create();
        wishListRepository.save(wishListEntity1);

        var wishListEntity2 = create();
        wishListRepository.save(wishListEntity2);


        int count = wishListRepository.listAll().size();
        Assertions.assertEquals(2, count);
    }

}