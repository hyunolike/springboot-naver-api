package com.example.naverapi.restaurant.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class WishListServiceTest {

    @Autowired
    private WishListService wishListService;

    @Test
    @DisplayName("검색 테스트")
    public void searchTest(){

        var result = wishListService.search("떡볶이");

        System.out.println(result);

        Assertions.assertNotNull(result);

    }

}