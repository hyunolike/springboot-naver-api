package com.example.naverapi.naver;

import com.example.naverapi.naver.dto.SearchImageReq;
import com.example.naverapi.naver.dto.SearchLocalReq;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class NaverClientTest {
    @Autowired
    private NaverClient naverClient;

    @Test
    @DisplayName("네이버 API 검색 테스트")
    public void localSearchTest(){
        SearchLocalReq search = SearchLocalReq.builder()
                .query("곱창")
                .display(5)
                .start(1)
                .sort("random")
                .build();
        var result = naverClient.localSearch(search);
        System.out.println(result);

    }

    @Test
    @DisplayName("네이버 API 이미지 테스트")
    public void localImageTest(){
        SearchImageReq search = SearchImageReq.builder()
                .query("곱창")
                .display(5)
                .start(1)
                .sort("sim")
                .build();
        var result = naverClient.iamgeSearch(search);
        System.out.println(result);

    }



}