package com.example.naverapi.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MemoryDbEntity {

    protected int index; // index 0으로 들어갈 수 있기 때문에 Integer 사용 !
}
