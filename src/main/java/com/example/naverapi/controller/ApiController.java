package com.example.naverapi.controller;

import com.example.naverapi.restaurant.dto.WishListDto;
import com.example.naverapi.restaurant.service.WishListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = {"API"})
@RestController
@RequestMapping("/api/restaurant")
@RequiredArgsConstructor
public class ApiController {

    private final WishListService wishListService;


    @ApiOperation(value = "검색 정보 리턴하는 메소드")
    @GetMapping("/search")
    public WishListDto search(@RequestParam String query){
        return wishListService.search(query);
    }

    @ApiOperation(value = "위시 리스트 추가 메소드")
    @PostMapping("")
    public WishListDto add(@RequestBody WishListDto wishListDto){
      log.info("{}", wishListDto);
      return wishListService.add(wishListDto);
    }

    @ApiOperation(value = "전체 조회 메소드")
    @GetMapping("/all")
    public List<WishListDto> findAll(){
        return wishListService.findAll();
    }

    @ApiOperation(value = "삭제 메소드")
    @DeleteMapping("/{index}")
    public void delete(@PathVariable int index){
        wishListService.delete(index);
    }

    @ApiOperation(value = "방문 체크 메소드")
    @PostMapping("/{index}")
    public void addVisit(@PathVariable int index){
        wishListService.addVisit(index);
    }
}
