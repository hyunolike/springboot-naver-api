package com.example.naverapi.restaurant.service;

import com.example.naverapi.naver.NaverClient;
import com.example.naverapi.naver.dto.SearchImageReq;
import com.example.naverapi.naver.dto.SearchLocalReq;
import com.example.naverapi.naver.dto.SearchLocalRes;
import com.example.naverapi.restaurant.dto.WishListDto;
import com.example.naverapi.restaurant.entity.WishListEntity;
import com.example.naverapi.restaurant.repository.WishListRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class WishListService {

    private final NaverClient naverClient;
    private final WishListRepository wishListRepository;

    public WishListDto search(String query){

        // 지역 검색
        var searchLocalReq = new SearchLocalReq();
        searchLocalReq.setQuery(query);

        var searchLocalRes = naverClient.localSearch(searchLocalReq);

        if(searchLocalRes.getTotal() > 0){
            var LocalItem = searchLocalRes.getItems().stream().findFirst().get();

            var imageQuery = LocalItem.getTitle().replaceAll("<[^>]*>", ""); // 문자열 처리
            var searchImageReq = new SearchImageReq();
            searchImageReq.setQuery(imageQuery);

            var searchImageRes = naverClient.iamgeSearch(searchImageReq);

            if(searchImageRes.getTotal() > 0){
                var imageitem = searchImageRes.getItems().stream().findFirst().get();

                WishListDto result = WishListDto.builder()
                        .title(LocalItem.getTitle())
                        .category(LocalItem.getCategory())
                        .address(LocalItem.getAddress())
                        .roadAddress(LocalItem.getRoadAddress())
                        .homePageLink(LocalItem.getLink())
                        .imageLink(imageitem.getLink())
                        .build();

                return result;
            }
        }
        return new WishListDto();

        // 이미지 검색


        // 결과

    }


    public WishListDto add(WishListDto wishListDto) {
        var entity = dtoToEntity(wishListDto);
        var saveEntity = wishListRepository.save(entity);
        return entityToDto(saveEntity);
    }

    private WishListEntity dtoToEntity(WishListDto wishListDto) {
        WishListEntity entity = WishListEntity.builder()
//                .index(wishListDto.getIndex())
                .title(wishListDto.getTitle())
                .category(wishListDto.getCategory())
                .address(wishListDto.getAddress())
                .roadAddress(wishListDto.getRoadAddress())
                .homePageLink(wishListDto.getHomePageLink())
                .imageLink(wishListDto.getImageLink())
                .isVisit(wishListDto.isVisit())
                .visitCount(wishListDto.getVisitCount())
                .lastVisitDate(wishListDto.getLastVisitDate())
                .build();
        return entity;

    }

    private WishListDto entityToDto(WishListEntity wishListEntity){
        WishListDto dto = WishListDto.builder()
                .index(wishListEntity.getIndex())
                .title(wishListEntity.getTitle())
                .category(wishListEntity.getCategory())
                .address(wishListEntity.getAddress())
                .roadAddress(wishListEntity.getRoadAddress())
                .homePageLink(wishListEntity.getHomePageLink())
                .imageLink(wishListEntity.getImageLink())
                .isVisit(wishListEntity.isVisit())
                .visitCount(wishListEntity.getVisitCount())
                .lastVisitDate(wishListEntity.getLastVisitDate())
                .build();
        return dto;
    }

    public List<WishListDto> findAll() {
        return wishListRepository.listAll().stream().map(it -> entityToDto(it)).collect(Collectors.toList());
    }

    public void delete(int index) {
        wishListRepository.deleteById(index);
    }

    public void addVisit(int index){
        var wishItem = wishListRepository.findById(index);
        if(wishItem.isPresent()){
            var item = wishItem.get();

            item.setVisit(true);
            item.setVisitCount(item.getVisitCount()+1);
        }
    }
}
