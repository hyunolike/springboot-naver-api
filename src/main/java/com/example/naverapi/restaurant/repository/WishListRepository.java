package com.example.naverapi.restaurant.repository;

import com.example.naverapi.db.MemoryDbRepositoryAbstract;
import com.example.naverapi.restaurant.entity.WishListEntity;
import org.springframework.stereotype.Repository;

@Repository
public class WishListRepository extends MemoryDbRepositoryAbstract<WishListEntity> {
}
